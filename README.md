<h1>FLTK - Demo</h1>

This project will eventually contain a collection of small FLTK demo
programs.

The only requirements to build these demo programs are FLTK and CMake.

You can even build with Visual Studio although the Linux build is
tested more frequently.
